/******************************************************************************
 * Copyright 2019 Manuel Simon
 * This file is part of the norman library.
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 *****************************************************************************/

//! Implementations of the norm traits for [`ndarray::ArrayBase`].

use num_traits::{Num, Float};
use ndarray::{ArrayBase, Data, Dimension};

use crate::{Norm, Distance};
use crate::desc::{Abs, Sup, PNorm, PNormReal};
use crate::utility::{supnorm_iterable, pnorm_iterable, pnorm_real_iterable};

impl<S, D, T, R> Norm<Sup> for ArrayBase<S, D>
where
    S: Data<Elem=T>,
    D: Dimension,
    T: Norm<Abs, Output = R>,
    R: Num + PartialOrd,
{
    type Output = <T as Norm<Abs>>::Output;
    fn norm(&self, _desc: Sup) -> <Self as Norm<Sup>>::Output {
        supnorm_iterable(self.into_iter().map(|a| a.norm(Abs::new())))
    }
}

impl<S, D, T, R> Norm<PNorm> for ArrayBase<S, D>
where
    S: Data<Elem=T>,
    D: Dimension,
    T: Norm<Abs, Output = R>,
    R: Float + From<f32>,
{
    type Output = <T as Norm<Abs>>::Output;
    fn norm(&self, desc: PNorm) -> <Self as Norm<Sup>>::Output {
        pnorm_iterable(self.into_iter().map(|a| a.norm(Abs::new())), desc)
    }
}

impl<S, D, T, R> Norm<PNormReal> for ArrayBase<S, D>
where
    S: Data<Elem=T>,
    D: Dimension,
    T: Norm<Abs, Output = R>,
    R: Float + From<f32>,
{
    type Output = <T as Norm<Abs>>::Output;
    fn norm(&self, desc: PNormReal) -> <Self as Norm<Sup>>::Output {
        pnorm_real_iterable(self.into_iter().map(|a| a.norm(Abs::new())), desc)
    }
}


impl<S, D, T, R> Distance<Sup> for ArrayBase<S, D>
where
    S: Data<Elem=T>,
    D: Dimension,
    T: Distance<Abs, Output = R>,
    R: Num + PartialOrd,
{
    type Output = <T as Distance<Abs>>::Output;
    fn distance(&self, other: &Self, _desc: Sup) -> <Self as Distance<Sup>>::Output {
        if let Some(broadcast) = other.broadcast(self.shape()) {
            supnorm_iterable(
                self.into_iter().zip(broadcast).map(|(a, b)| a.distance(b, Abs::new()))
            )
        } else if let Some(broadcast) = self.broadcast(other.shape()) {
            supnorm_iterable(
                broadcast.into_iter().zip(other).map(|(a, b)| a.distance(b, Abs::new()))
            )
        } else {
            panic!(
                "Could not broadcast arrays together: shape: {:?} and shape: {:?}.",
                other.shape(), self.shape()
            );
        }
    }
}

impl<S, D, T, R> Distance<PNorm> for ArrayBase<S, D>
where
    S: Data<Elem=T>,
    D: Dimension,
    T: Distance<Abs, Output = R>,
    R: Float + From<f32>,
{
    type Output = <T as Distance<Abs>>::Output;
    fn distance(&self, other: &Self, desc: PNorm) -> <Self as Distance<Sup>>::Output {
        if let Some(broadcast) = other.broadcast(self.shape()) {
            pnorm_iterable(
                self.into_iter().zip(broadcast).map(|(a, b)| a.distance(b, Abs::new())),
                desc
            )
        } else if let Some(broadcast) = self.broadcast(other.shape()) {
            pnorm_iterable(
                broadcast.into_iter().zip(other).map(|(a, b)| a.distance(b, Abs::new())),
                desc
            )
        } else {
            panic!(
                "Could not broadcast arrays together: shape: {:?} and shape: {:?}.",
                other.shape(), self.shape()
            );
        }
    }
}

impl<S, D, T: std::fmt::Debug, R> Distance<PNormReal> for ArrayBase<S, D>
where
    S: Data<Elem=T>,
    D: Dimension,
    T: Distance<Abs, Output = R>,
    R: Float + From<f32>,
{
    type Output = <T as Distance<Abs>>::Output;
    fn distance(&self, other: &Self, desc: PNormReal) -> <Self as Distance<Sup>>::Output {
        if let Some(broadcast) = other.broadcast(self.shape()) {
            pnorm_real_iterable(
                self.into_iter().zip(broadcast).map(|(a, b)| a.distance(b, Abs::new())),
                desc
            )
        } else if let Some(broadcast) = self.broadcast(other.shape()) {
            pnorm_real_iterable(
                broadcast.into_iter().zip(other).map(|(a, b)| a.distance(b, Abs::new())),
                desc
            )
        } else {
            panic!(
                "Could not broadcast arrays together: shape: {:?} and shape: {:?}.",
                other.shape(), self.shape()
            );
        }
    }
}

#[cfg(test)]
mod tests {
    use ndarray::Array1;

    use crate::{Norm, Distance};
    use crate::desc::{Sup, PNorm, PNormReal};

    #[test]
    fn supnorm_ndarray() {
        let a = Array1::from(vec![3.0f32, -4.0, 2.0]);
        assert_eq!(a.norm(Sup::new()), 4.0);
        let b = Array1::from(vec![2.0f32, 2.0, 2.0]);
        assert_eq!(a.distance(&b, Sup::new()), 6.0);
        let c = Array1::from(vec![2.0f32]);
        assert_eq!(a.distance(&c, Sup::new()), 6.0);
        assert_eq!(c.distance(&a, Sup::new()), 6.0);
    }

    #[test]
    fn pnorm_ndarray() {
        let a = Array1::from(vec![3.0f32, -4.0, 2.0]);
        assert_eq!(a.norm(PNorm::new(2)), 29.0f32.sqrt());
        let b = Array1::from(vec![2.0f32, 2.0, 2.0]);
        assert_eq!(a.distance(&b, PNorm::new(2)), 37.0f32.sqrt());
        let c = Array1::from(vec![2.0f32]);
        assert_eq!(a.distance(&c, PNorm::new(2)), 37.0f32.sqrt());
        assert_eq!(c.distance(&a, PNorm::new(2)), 37.0f32.sqrt());
    }

    #[test]
    fn pnorm_real_ndarray() {
        let a = Array1::from(vec![3.0f32, -4.0, 2.0]);
        assert_eq!(a.norm(PNormReal::from_f32(2.0)), 29.0f32.sqrt());
        let b = Array1::from(vec![2.0f32, 2.0, 2.0]);
        assert_eq!(a.distance(&b, PNormReal::from_f32(2.0)), 37.0f32.sqrt());
        let c = Array1::from(vec![2.0f32]);
        assert_eq!(a.distance(&c, PNormReal::from_f32(2.0)), 37.0f32.sqrt());
        assert_eq!(c.distance(&a, PNormReal::from_f32(2.0)), 37.0f32.sqrt());
    }

    #[test]
    #[should_panic]
    fn distance_supnorm_nofit() {
        let a: Array1<f32> = Array1::ones([3]);
        let b: Array1<f32> = Array1::ones([2]);
        a.distance(&b, Sup::new());
    }

    #[test]
    #[should_panic]
    fn distance_pnorm_nofit() {
        let a: Array1<f32> = Array1::ones([3]);
        let b: Array1<f32> = Array1::ones([2]);
        a.distance(&b, PNorm::new(3));
    }

    #[test]
    #[should_panic]
    fn distance_pnorm_real_nofit() {
        let a: Array1<f32> = Array1::ones([3]);
        let b: Array1<f32> = Array1::ones([2]);
        a.distance(&b, PNormReal::from_f32(3.0));
    }
}