/******************************************************************************
 * Copyright 2019 Manuel Simon
 * This file is part of the norman library.
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 *****************************************************************************/

//! Implementations of the norm traits for [`noisy_float::NoisyFloat`].

use num_traits::Float;
use noisy_float::{NoisyFloat, FloatChecker};

use crate::desc::Abs;

use crate::{Norm, Distance};

impl<F: Float, C: FloatChecker<F>> Norm<Abs> for NoisyFloat<F, C> {
    type Output = Self;
    fn norm(&self, _desc: Abs) -> Self {
        self.abs()
    }
}

impl<F: Float, C: FloatChecker<F>> Distance<Abs> for NoisyFloat<F, C> {
    type Output = Self;
    fn distance(&self, other: &Self, _desc: Abs) -> Self {
        (*self - *other).abs()
    }
}

#[cfg(test)]
mod tests {
    use noisy_float::types::{r32, r64};

    use crate::{Norm, Distance};
    use crate::desc::Abs;

    #[test]
    fn noisy_float_norm() {
        assert_eq!(r32(4.0).norm(Abs::new()), 4.0);
        assert_eq!(r64(-4.0).norm(Abs::new()), 4.0);
    }

    #[test]
    fn noisy_float_distance() {
        assert_eq!(r32(4.0).distance(&r32(2.0), Abs::new()), 2.0);
        assert_eq!(r64(4.0).distance(&r64(7.0), Abs::new()), 3.0);
    }
}