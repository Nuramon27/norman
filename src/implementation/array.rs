/******************************************************************************
 * Copyright 2019 Manuel Simon
 * This file is part of the norman library.
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 *****************************************************************************/

use num_traits::{Num, Float};

use crate::{Norm, Distance};
use crate::desc::{Abs, Sup, PNorm, PNormReal};
use crate::utility::{supnorm_iterable, pnorm_iterable, pnorm_real_iterable};

/// Implements the `Norm` and `Distance` traits for arrays with n elements
/// whose base type implements `Norm<Abs>`.
macro_rules! impl_array {
    ( $( $n:literal )* ) => ($(
        impl<T: Norm<Abs, Output = R>, R: Num + PartialOrd> Norm<Sup> for [T; $n] {
            type Output = <T as Norm<Abs>>::Output;
            fn norm(&self, _desc: Sup) -> <Self as Norm<Sup>>::Output {
                supnorm_iterable(self.into_iter().map(|a| a.norm(Abs::new())))
            }
        }

        impl<T: Norm<Abs, Output = R>, R: Float + From<f32>> Norm<PNorm> for [T; $n] {
            type Output = <T as Norm<Abs>>::Output;
            fn norm(&self, desc: PNorm) -> <Self as Norm<PNorm>>::Output {
                pnorm_iterable(self.into_iter().map(|a| a.norm(Abs::new())), desc)
            }
        }

        impl<T: Norm<Abs, Output = R>, R: Float + From<f32>> Norm<PNormReal> for [T; $n] {
            type Output = <T as Norm<Abs>>::Output;
            fn norm(&self, desc: PNormReal) -> <Self as Norm<PNormReal>>::Output {
                pnorm_real_iterable(self.into_iter().map(|a| a.norm(Abs::new())), desc)
            }
        }

        impl<T: Distance<Abs, Output = R>, R: Num + PartialOrd> Distance<Sup> for [T; $n] {
            type Output = <T as Distance<Abs>>::Output;
            fn distance(&self, other: &Self, _desc: Sup) -> <Self as Distance<Sup>>::Output {
                assert_eq!(
                    self.len(), other.len(),
                    "Sizes of Vecs do not match. Left: {}; Right: {}.",
                    self.len(), other.len()
                );
                supnorm_iterable(self.into_iter().zip(other).map(|(a, b)| a.distance(b, Abs::new())))
            }
        }

        impl<T: Distance<Abs, Output = R>, R: Float + From<f32>> Distance<PNorm> for [T; $n] {
            type Output = <T as Distance<Abs>>::Output;
            fn distance(&self, other: &Self, desc: PNorm) -> <Self as Distance<PNorm>>::Output {
                assert_eq!(
                    self.len(), other.len(),
                    "Sizes of Vecs do not match. Left: {}; Right: {}.",
                    self.len(), other.len()
                );
                pnorm_iterable(self.into_iter().zip(other).map(|(a, b)| a.distance(b, Abs::new())), desc)
            }
        }

        impl<T: Distance<Abs, Output = R>, R: Float + From<f32>> Distance<PNormReal> for [T; $n] {
            type Output = <T as Distance<Abs>>::Output;
            fn distance(&self, other: &Self, desc: PNormReal) -> <Self as Distance<PNormReal>>::Output {
                assert_eq!(
                    self.len(), other.len(),
                    "Sizes of Vecs do not match. Left: {}; Right: {}.",
                    self.len(), other.len()
                );
                pnorm_real_iterable(self.into_iter().zip(other).map(|(a, b)| a.distance(b, Abs::new())), desc)
            }
        }
    )*)
}

impl_array!(
     0
     1  2  3  4  5  6  7  8
     9 10 11 12 13 14 15 16
    17 18 19 20 21 22 23 24
    25 26 27 28 29 30 31 32
);

#[cfg(test)]
mod tests {
    use crate::{Norm, Distance};
    use crate::desc::{Sup, PNorm, PNormReal};

    #[test]
    fn supnorm_empty_array() {
        let a: [f64; 0] = [];
        assert_eq!(a.norm(Sup::new()), 0.0);
        let b: [f64; 0] = [];
        assert_eq!(a.distance(&b, Sup::new()), 0.0);
    }

    #[test]
    fn supnorm_array() {
        let a = [3.0f32, -4.0, 2.0];
        assert_eq!(a.norm(Sup::new()), 4.0);
        let b = [2.0f32, 2.0, 2.0];
        assert_eq!(a.distance(&b, Sup::new()), 6.0);
    }

    #[test]
    fn pnorm_array() {
        let a = [3.0f32, -4.0, 2.0];
        assert_eq!(a.norm(PNorm::new(2)), 29.0f32.sqrt());
        let b = [2.0f32, 2.0, 2.0];
        assert_eq!(a.distance(&b, PNorm::new(2)), 37.0f32.sqrt());
    }

    #[test]
    fn pnorm_real_array() {
        let a = [3.0f32, -4.0, 2.0];
        assert_eq!(a.norm(PNormReal::from_f32(2.0)), 29.0f32.sqrt());
        let b = [2.0f32, 2.0, 2.0];
        assert_eq!(a.distance(&b, PNormReal::from_f32(2.0)), 37.0f32.sqrt());
    }
}