/******************************************************************************
 * Copyright 2019 Manuel Simon
 * This file is part of the norman library.
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 *****************************************************************************/

//! Implementations of norms for the complex number types in [`num_complex`].

use num_traits::Float;
use num_complex::Complex;

use crate::{Norm, Distance};
use crate::desc::{Abs, Sup};

impl<T: Float> Norm<Abs> for Complex<T> {
    type Output = T;
    /// Calculates the usual euclidean norm of the complex number.
    fn norm(&self, _desc: Abs) -> T {
        Complex::norm(*self)
    }
}

impl<T: Float> Norm<Sup> for Complex<T> {
    type Output = T;
    /// Calculates the supremum norm of the complex number,
    /// i.e. the maximum of the absolute values of real and imaginary part.
    fn norm(&self, _desc: Sup) -> T {
        self.re.abs().max(self.im.abs())
    }
}

impl<T: Float> Distance<Abs> for Complex<T> {
    type Output = T;
    /// Calculates the usual euclidean norm of the complex number.
    fn distance(&self, other: &Self, _desc: Abs) -> T {
        Complex::norm(self - other)
    }
}

impl<T: Float> Distance<Sup> for Complex<T> {
    type Output = T;
    /// Calculates the supremum norm of the complex number,
    /// i.e. the maximum of the absolute values of real and imaginary part.
    fn distance(&self, other: &Self, _desc: Sup) -> T {
        (self.re - other.re).abs().max((self.im - other.im).abs())
    }
}

#[cfg(test)]
mod tests {
    use num_complex::Complex;

    use crate::{Norm, Distance};
    use crate::desc::{Abs, Sup};

    #[test]
    fn complex_norm() {
        assert_eq!(Norm::norm(&Complex::new( 3.0f32, -4.0f32), Abs::new()), 5.0);
        assert_eq!(Norm::norm(&Complex::new(-4.0f64, -3.0f64), Abs::new()), 5.0);
    }

    #[test]
    fn complex_sup_norm() {
        assert_eq!(Norm::norm(&Complex::new( 4.0f32, -8.0f32), Sup::new()), 8.0);
        assert_eq!(Norm::norm(&Complex::new( 3.0f32, -1.0f32), Sup::new()), 3.0);
    }

    #[test]
    fn complex_distance() {
        assert_eq!(Distance::distance(
            &Complex::new( 3.0f32, -4.0f32),
            &Complex::new( 3.0f32,  3.0f32),
            Abs::new()
            ), 7.0);
        assert_eq!(Distance::distance(
            &Complex::new( 5.0f32,  0.0f32),
            &Complex::new( 1.0f32,  3.0f32),
            Abs::new()
            ), 5.0);
    }
}