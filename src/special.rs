/******************************************************************************
 * Copyright 2019 Manuel Simon
 * This file is part of the norman library.
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 *****************************************************************************/

//! Dedicated traits for some _standard norms_ like the euclidean norm.
//!
//! Always calling the `norm` function of the [`Norm`](crate::Norm) trait
//! with an additional [`Abs::new`](crate::desc::Abs::new) or
//! [`PNorm::eucl`](crate::desc::PNorm::eucl) argument may be a bit
//! verbose if one only wants to get some grasp of how big a value is. So this
//! module contains some specialized traits that calculate standard
//! norms without an additional norm descriptor.
//!
//! The same goes for the [`Distance`](crate::Distance) trait.
//!
//! Currently there are only [`NormEucl`](special::NormEucl)
//! and [`DistanceEucl`](special::DistanceEucl).
//!
//! For details on their implementation, see the module documentation
//! of [`implementation`].

pub mod implementation;

use std::ops::{Div, DivAssign};

use num_traits::Num;

/// This trait is used to emphasize a special norm for a type of vectors
/// that is kind of a euclidean norm and can be used as the standard
/// norm for this type.
pub trait NormEucl {
    /// The resulting type of the norm function.
    type Output: Num;
    /// Calculates the euclidean norm of `self`.
    ///
    /// # Panics
    ///
    /// An implementation of `norm_eucl` should never panic.
    ///
    /// An exception may be made for types like the [`noisy_float`]
    /// floating point types that already have a special panicking behaviour
    /// to ensure that no invalid values occur.
    fn norm_eucl(&self) -> <Self as NormEucl>::Output;
}

/// This trait is used to emphasize a special distance function for a type of vectors
/// that is kind of a euclidean distance and can be used as the standard
/// norm for this type.
pub trait DistanceEucl {
    /// The resulting type of the distance function.
    type Output: Num;
    /// Calculates the euclidean distance between `self` and `other`.
    ///
    /// # Panics
    ///
    /// An implementation of `distance_eucl` may panic if the operands
    /// do not fit together, e.g. have different sizes etc.
    fn distance_eucl(&self, other: &Self) -> <Self as DistanceEucl>::Output;
}


/// Normalizes the vector `v` according to the euclidean norm,
/// i.e. divides it by its norm.
///
/// As long as the implementations of `Div` and `DivAssign` on `T` match,
/// `v` will be equal to `normalized(v)` after calling this function.
///
/// ## Attention
///
/// Due to numerical errors, `v` is **not** guaranteed to have exactly norm `1`
/// after calling this function.
///
/// On integer types this function will do complete nonsense since
/// `DivAssign` is implemented as an integer division for integers.
pub fn normalize_eucl<T: NormEucl<Output=R> + DivAssign<R>, R: Num>(v: &mut T) {
    *v /= v.norm_eucl();
}

/// Returns the normalization of `v` according to the euclidean norm,
/// i.e. `v` divided by its norm.
///
/// ## Attention
///
/// Due to numerical errors, the result is **not** guaranteed to have exactly norm `1`
/// after calling this function.
///
/// On integer types this function will do complete nonsense since
/// `Div` is implemented as an integer division for integers.
pub fn normalized_eucl<T: NormEucl<Output=R> + Div<R, Output=T>, R: Num>(v: T) -> T {
    let norm = v.norm_eucl();
    v / norm
}