# Norman

The norman library provides everything you need for calculationg norms of elements
of vector spaces.

Based on two traits—`Norm` and `Distance`
– this crate implements different kinds of norm and distance
functions for a wide variety of types—including complex numbers and arrays.

These traits are mainly meant to facilitate generic implementations
of numeric algorithms, where one often needs a grasp of how _big_
a value is or how _far_ two values are apart.

## Usage

Add this to your `Cargo.toml`:

```toml
[dependencies]
norman = "0.0.4"
```

Then you can calculate the euclidean distance between two
complex numbers:

```rust
use num_complex::Complex;

use norman::Distance;
use norman::desc::Abs;

assert_eq!(Complex::new(2.0, 5.0).distance(&Complex::new(-1.0, 1.0), Abs::new()), 5.0);
```

or the supremum norm, 2-norm and 1-norm of a vector that is rerpesented by an ndarray:

```rust
use ndarray::Array1;

use norman::Norm;
use norman::desc::{Sup, PNorm};

let a = Array1::from(vec![2.0f32, -4.0, -2.0]);

assert_eq!(a.norm(Sup::new()), 4.0);
assert_eq!(a.norm(PNorm::new(2)), (2.0f32*2.0 + 4.0*4.0 + 2.0*2.0).sqrt());
assert_eq!(a.norm(PNorm::new(1)), 2.0f32 + 4.0 + 2.0);
```

The minimum supported version of rustc is currently **1.34.2**.

## Crate stability

The main reason why this crate is in pre-0.1-state is that I am not sure about
the naming of several items, so it may still encounter some renames.
I am particularly not very happy with the verbose name `norm_eucl` for
“just calulate the usual standard norm“.

## Future development

The first step will be to support types representing usual vectors
which are around in the ecosystem like:

* vector types of [rulinalg](https://crates.io/crates/rulinalg),
[nalgebra](https://crates.io/crates/nalgebra) and co.

* simd types from [packed_simd](https://crates.io/crates/packed_simd)

Later I will maybe introduce some operator norms on matrices.

Furthermore, I might think about adding a new trait for derivatives of norms –
in some algorithms, you do not only need to know the exact value of a norm,
but also the derivative in order to know in which direction to go
if you want to minimize the norm.

# License

Norman is distributed under the terms of both the MIT license and the
Apache License (Version 2.0).

See [LICENSE-APACHE](LICENSE-APACHE) and [LICENSE-MIT](LICENSE-MIT), and
[COPYING](COPYING) for details.

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
